/*
** my_getnbr.c for my_getnbr.c in /home/el-mou_r/rendu/Piscine_C_J07/lib
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed Oct  7 12:12:31 2015 Raidouane EL MOUKHTARI
** Last update Thu Nov 12 02:09:32 2015 Raidouane EL MOUKHTARI
*/

int	get_nbr1(char *str, int a, int na)
{
  int	nb;
  int	s;
  int	m;

  nb = 0;
  while (str && str[a] && str[a] >= '0' && str[a] <= '9')
    {
      s = nb;
      if (s > (nb * 10 + str[a] - '0'))
        {
          return (0);
        }
      nb = nb * 10 + (str[a] - '0');
      a = a + 1;
    }
  m = na % 2;
  if (m == 1)
    {
      nb = nb * -1;
    }
  return (nb);
}

int	my_getnbr(char *str)
{
  int	a;
  int	nb;
  int	na;

  a = 0;
  na = 0;
  while (str && str[a])
    {
      if (str[a] == '-')
        {
          na = na + 1;
        }
      if (str[a] < 48 || str[a] > 57)
	{
	  return (0);
	}
      if (str[a] >= '0' && str[a] <= '9')
        {
          nb = get_nbr1(str, a, na);
          return (nb);
        }
      a = a + 1;
    }
  return (0);
}
