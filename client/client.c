/*
** client.c for client.c in /home/el-mou_r/rendu/PSU/PSU_2015_minitalk
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Feb  1 14:56:16 2016 Raidouane EL MOUKHTARI
** Last update Thu Feb 18 17:41:24 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

unsigned int	get_val_msg(int i, u_charbin charbin)
{
  unsigned int	val;

  if (i == 1)
    val = charbin.bin.one;
  else if (i == 2)
    val = charbin.bin.two;
  else if (i == 3)
    val = charbin.bin.three;
  else if (i == 4)
    val = charbin.bin.four;
  else if (i == 5)
    val = charbin.bin.five;
  else if (i == 6)
    val = charbin.bin.six;
  else if (i == 7)
    val = charbin.bin.seven;
  else if (i == 8)
    val = charbin.bin.eight;
  return (val);
}

int	my_static(int val, int pass)
{
  static int	t = -1;

  if (val != -1 && pass == 1)
    t = val;
  return (t);
}

void		 client(int pid, char *str)
{
  int		i;
  u_charbin	charbin;
  unsigned int	val;
  int		t;
  int		pass;

  t = 0;
  while (t <= my_strlen(str))
    {
      charbin.c = str[t];
      i = 1;
      while (i <= 8)
	{
	  usleep(0);
	  val = get_val_msg(i, charbin);
	  (val == 1)? (kill(pid, SIGUSR1)) : (kill(pid, SIGUSR2));
	  while ((pass = my_static(-1, 0)) != 1);
	  if (pass == 1)
	    my_static(0, 1);
	  i++;
	}
      t++;
    }
}

void	handl(int sign)
{
  if (sign == SIGUSR1)
    my_static(1, 1);
  else
    exit_function();
}

int	main(int ac, char **av)
{
  int	pass;

  if (ac != 3)
    return (-1);
  signal(SIGUSR1, handl);
  signal(SIGUSR2, handl);
  send_pid_client(my_getnbr(av[1]));
  while ((pass = my_static(-1, 0)) != 1);
  if (pass == 1)
    my_static(0, 1);
  client(my_getnbr(av[1]), av[2]);
  return (0);
}
