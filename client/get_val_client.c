/*
** get_val_client.c for get_val_client.c in /home/el-mou_r/rendu/PSU/PSU_2015_minitalk
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed Feb  3 01:50:07 2016 Raidouane EL MOUKHTARI
** Last update Fri Feb 19 19:04:42 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int		get_val_firstpart(int i, u_decbin *decbin)
{
  unsigned int	val;

  if (i == 1)
    val = decbin->bin.one;
  else if (i == 2)
    val = decbin->bin.two;
  else if (i == 3)
    val = decbin->bin.three;
  else if (i == 4)
    val = decbin->bin.four;
  else if (i == 5)
    val = decbin->bin.five;
  else if (i == 6)
    val = decbin->bin.six;
  else if (i == 7)
    val = decbin->bin.seven;
  else if (i == 8)
    val = decbin->bin.eight;
  else if (i == 9)
    val = decbin->bin.nine;
  else if (i == 10)
    val = decbin->bin.ten;
  else if (i == 11)
    val = decbin->bin.eleven;
  return (val);
}

int		get_val_secondpart(int i, u_decbin *decbin)
{
  unsigned int	val;

  if (i == 12)
    val = decbin->bin.twelve;
  else if (i == 13)
    val = decbin->bin.thirteen;
  else if (i == 14)
    val = decbin->bin.fourteen;
  else if (i == 15)
    val = decbin->bin.fiveteen;
  else if (i == 16)
    val = decbin->bin.sixteen;
  else if (i == 17)
    val = decbin->bin.seventeen;
  else if (i == 18)
    val = decbin->bin.eighteen;
  else if (i == 19)
    val = decbin->bin.nineteen;
  else if (i == 20)
    val = decbin->bin.twenty;
  else if (i == 21)
    val = decbin->bin.twenty_one;
  else if (i == 22)
    val = decbin->bin.twenty_two;
  return (val);
}

int		get_val_thirdpart(int i, u_decbin *decbin)
{
  unsigned int	val;

  if (i == 23)
    val = decbin->bin.twenty_three;
  else if (i == 24)
    val = decbin->bin.twenty_four;
  else if (i == 25)
    val = decbin->bin.twenty_five;
  else if (i == 26)
    val = decbin->bin.twenty_six;
  else if (i == 27)
    val = decbin->bin.twenty_seven;
  else if (i == 28)
    val = decbin->bin.twenty_eight;
  else if (i == 29)
    val = decbin->bin.twenty_nine;
  else if (i == 30)
    val = decbin->bin.thirty;
  else if (i == 31)
    val = decbin->bin.thirty_one;
  else if (i == 32)
    val = decbin->bin.thirty_two;
  return (val);
}

void		send_pid_client(int pid_server)
{
  int		i;
  u_decbin	decbin;
  unsigned int	val;

  decbin.client_pid = getpid();
  i = 1;
  while (i <= 32)
    {
      if (i <= 11)
        val = get_val_firstpart(i, &decbin);
      else if (i > 11 && i <= 22)
        val = get_val_secondpart(i, &decbin);
      else if (i > 22)
        val = get_val_thirdpart(i, &decbin);
      if (val == 1)
        kill(pid_server, SIGUSR1);
      else
        kill(pid_server, SIGUSR2);
      if (i == 32)
	return ;
      usleep(10000);
      i++;
    }
}
