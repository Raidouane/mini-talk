/*
** exit_function.c for exit_function.c in /home/el-mou_r/rendu/PSU/PSU_2015_minitalk
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Feb 18 17:23:22 2016 Raidouane EL MOUKHTARI
** Last update Thu Feb 18 17:26:04 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	exit_function()
{
  my_putstr("Error PID\n");
  exit (-1);
}
