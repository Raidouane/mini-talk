##
## Makefile for Makefile in /home/el-mou_r/rendu/PSU/PSU_2015_minitalk
## 
## Made by Raidouane EL MOUKHTARI
## Login   <el-mou_r@epitech.net>
## 
## Started on  Mon Feb  1 14:53:33 2016 Raidouane EL MOUKHTARI
## Last update Thu Feb 18 17:40:32 2016 Raidouane EL MOUKHTARI
##

CC	= gcc

RM	= rm

NAME	= server/server

NAME2	= client/client

CFLAGS	= -I./include -W -Wall -pedantic

SRC	=server/server.c			\
	server/my_signal_functions.c		\
	server/recup_pid_client.c		\
	server/my_put_nbr.c			\
	server/my_putstr.c			\
	server/my_putchar.c

SRC2	=client/client.c		\
	client/get_val_client.c		\
	client/exit_function.c		\
	client/my_getnbr.c		\
	client/my_strlen.c		\
	client/my_putstr.c		\
	client/my_putchar.c

OBJ	= $(SRC:.c=.o)

OBJ2	= $(SRC2:.c=.o)

$(NAME): 	$(OBJ)	$(OBJ2)
	$(CC) $(OBJ) -o $(NAME)
	$(CC) $(OBJ2) -o $(NAME2)

all:	$(NAME)

clean:
	$(RM) -f $(OBJ) $(OBJ2)

fclean:	clean
	$(RM) -f $(NAME) $(NAME2)

re:	fclean all
