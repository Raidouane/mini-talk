/*
** my.h for my.h in /home/el-mou_r/rendu/PSU/PSU_2015_minitalk
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed Feb 17 23:47:45 2016 Raidouane EL MOUKHTARI
** Last update Thu Feb 18 17:27:18 2016 Raidouane EL MOUKHTARI
*/

#ifndef MY_H_
# define MY_H_
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

int	g_stop;

typedef struct	s_decbin
{
  unsigned int	one : 1;
  unsigned int	two : 1;
  unsigned int	three : 1;
  unsigned int	four : 1;
  unsigned int	five : 1;
  unsigned int	six : 1;
  unsigned int	seven : 1;
  unsigned int	eight : 1;
  unsigned int	nine : 1;
  unsigned int	ten : 1;
  unsigned int	eleven : 1;
  unsigned int	twelve: 1;
  unsigned int	thirteen : 1;
  unsigned int	fourteen : 1;
  unsigned int	fiveteen : 1;
  unsigned int	sixteen : 1;
  unsigned int	seventeen : 1;
  unsigned int	eighteen : 1;
  unsigned int	nineteen : 1;
  unsigned int	twenty : 1;
  unsigned int	twenty_one : 1;
  unsigned int	twenty_two: 1;
  unsigned int	twenty_three : 1;
  unsigned int	twenty_four : 1;
  unsigned int	twenty_five : 1;
  unsigned int	twenty_six : 1;
  unsigned int	twenty_seven : 1;
  unsigned int	twenty_eight : 1;
  unsigned int	twenty_nine : 1;
  unsigned int	thirty : 1;
  unsigned int	thirty_one : 1;
  unsigned int	thirty_two : 1;
}		t_decbin;

typedef struct	s_struct
{
  unsigned int	one : 1;
  unsigned int	two : 1;
  unsigned int	three : 1;
  unsigned int	four : 1;
  unsigned int	five : 1;
  unsigned int	six : 1;
  unsigned int	seven : 1;
  unsigned int	eight : 1;
}		t_bin;

typedef union	u_union
{
  t_bin		bin;
  char		c;
}		u_charbin;

typedef union	u_union1
{
  t_decbin	bin;
  unsigned int	client_pid;
}		u_decbin;

void		recup_client_pid(u_decbin *decbin);
void		fill_pid_client_firstpart(unsigned int bool, int i, u_decbin *decbin);
void		fill_pid_client_secondpart(unsigned int bool, int i, u_decbin *decbin);
void		fill_pid_client_lastpart(unsigned int bool, int i, u_decbin *decbin);
void		do_firstcond(int i, u_decbin *decbin);
void		hand(int );
void		lauch_server(u_decbin *);
int		lauch_server_conditon(u_charbin *charbin, int t);
int		lauch_server_secondconditon(u_charbin *charbin, int t);
int		fill_my_field_bits(unsigned int bool, int i, u_charbin *charbin);
unsigned int	send_val_firstpart(int pid_server, int i, u_decbin *decbin);
unsigned int	send_val_secondpart(int pid_server, int i, u_decbin *decbin);
unsigned int	send_val_thirdpart(int pid_server, int i, u_decbin *decbin);
void		send_pid_client(int pid_server);
unsigned int	get_val_msg(int i, u_charbin charbin);
void		client(int pid, char *str);
void		handl(int pid);
int		my_signal(int val, int pass);
void		handler(int sign);
void		my_putchar(char );
void		my_putstr(char *);
void		my_putnbr(int );
int		my_strlen(char *);
int		my_getnbr(char *);
int		exit_function();

#endif /* !MY_H_ */
