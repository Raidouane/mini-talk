/*
** my_put_nbr.c for my_put_nbr.c in /home/el-mou_r/rendu/Piscine_C_bistromathique/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sat Oct 31 01:30:36 2015 Raidouane EL MOUKHTARI
** Last update Thu Feb 18 14:59:08 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void		my_putnbr(int nb)
{
  int		mod;

  if (nb < 0)
    {
      my_putchar('-');
      nb  = nb * (-1);
    }
  mod = nb % 10;
  if (nb >= 10)
    my_putnbr(nb / 10);
  my_putchar(mod + 48);
}
