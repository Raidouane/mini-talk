/*
** my_putchar.c for my_putchar.c in /home/el-mou_r/rendu/PSU/PSU_2015_minitalk
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Feb  1 15:15:00 2016 Raidouane EL MOUKHTARI
** Last update Thu Feb 18 15:00:31 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	my_putchar(char c)
{
  write(1, &c, 1);
}

