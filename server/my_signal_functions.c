/*
** my_signal_functions.c for my_signal_functions.c in /home/el-mou_r/rendu/PSU/PSU_2015_minitalk
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Feb  9 02:18:34 2016 Raidouane EL MOUKHTARI
** Last update Tue Feb  9 02:18:36 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int		my_signal(int val, int pass)
{
  static int	t = -1;

  if (pass == 1)
    t = val;
  return (t);
}

void	handler(int sign)
{
  if (sign == SIGUSR1)
    my_signal(1, 1);
  if (sign == SIGUSR2)
    my_signal(0, 1);
}
