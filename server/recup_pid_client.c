/*
** recup_pid_client.c for recup_pid_client.c in /home/el-mou_r/rendu/PSU/PSU_2015_minitalk
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed Feb  3 01:24:01 2016 Raidouane EL MOUKHTARI
** Last update Thu Feb 18 14:58:29 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	fill_pid_client_lastpart(unsigned int bool, int i, u_decbin *decbin)
{
  if (i == 25)
    decbin->bin.twenty_five = bool;
  else if (i == 26)
    decbin->bin.twenty_six = bool;
  else if (i == 27)
    decbin->bin.twenty_seven = bool;
  else if (i == 28)
    decbin->bin.twenty_eight = bool;
  else if (i == 29)
    decbin->bin.twenty_nine= bool;
  else if (i == 30)
    decbin->bin.thirty = bool;
  else if (i == 31)
    decbin->bin.thirty_one = bool;
  else if (i == 32)
    decbin->bin.thirty_two = bool;
}

void	fill_pid_client_secondpart(unsigned int bool, int i, u_decbin *decbin)
{
  if (i == 13)
    decbin->bin.thirteen = bool;
  else if (i == 14)
    decbin->bin.fourteen = bool;
  else if (i == 15)
    decbin->bin.fiveteen = bool;
  else if (i == 16)
    decbin->bin.sixteen = bool;
  else if (i == 17)
    decbin->bin.seventeen = bool;
  else if (i == 18)
    decbin->bin.eighteen = bool;
  else if (i == 19)
    decbin->bin.nineteen = bool;
  else if (i == 20)
    decbin->bin.twenty = bool;
  else if (i == 21)
    decbin->bin.twenty_one = bool;
  else if (i == 22)
    decbin->bin.twenty_two = bool;
  else if (i == 23)
    decbin->bin.twenty_three = bool;
  else if (i == 24)
    decbin->bin.twenty_four = bool;
}

void	fill_pid_client_firstpart(unsigned int bool, int i, u_decbin *decbin)
{
  if (i == 1)
    decbin->bin.one = bool;
  else if (i == 2)
    decbin->bin.two = bool;
  else if (i == 3)
    decbin->bin.three = bool;
  else if (i == 4)
    decbin->bin.four = bool;
  else if (i == 5)
    decbin->bin.five = bool;
  else if (i == 6)
    decbin->bin.six = bool;
  else if (i == 7)
    decbin->bin.seven = bool;
  else if (i == 8)
    decbin->bin.eight = bool;
  else if (i == 9)
    decbin->bin.nine = bool;
  else if (i == 10)
    decbin->bin.ten = bool;
  else if (i == 11)
    decbin->bin.eleven = bool;
  else if (i == 12)
    decbin->bin.twelve = bool;
}

void	do_firstcond(int i, u_decbin *decbin)
{
  if (i <= 12)
    fill_pid_client_firstpart(1, i, decbin);
  else if (i > 12 && i <= 24)
    fill_pid_client_secondpart(1, i, decbin);
  else if (i > 24 && i <= 32)
    fill_pid_client_lastpart(1, i, decbin);
  if (i == 32)
    {
      kill(decbin->client_pid, SIGUSR2);
      g_stop = 1;
    }
}

void		recup_client_pid(u_decbin *decbin)
{
  static int	i = 0;

  if (my_signal(0, 0) == 1 && i <= 32)
    {
      i++;
      do_firstcond(i, decbin);
      my_signal(-1, 1);
    }
  else if (my_signal(0, 0) == 0 && i <= 32)
    {
      i++;
      if (i <= 12)
        fill_pid_client_firstpart(0, i, decbin);
      else if (i > 12 && i <= 24)
        fill_pid_client_secondpart(0, i, decbin);
      else if (i > 24 && i <= 32)
        fill_pid_client_lastpart(0, i, decbin);
      if (i == 32)
        {
          g_stop = 1;
          kill(decbin->client_pid, SIGUSR1);
	  i = 0;
        }
      my_signal(-1, 1);
    }
}
