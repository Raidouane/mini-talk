/*
** server.c for server.c in /home/el-mou_r/rendu/PSU/PSU_2015_minitalk
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Feb  1 15:10:16 2016 Raidouane EL MOUKHTARI
** Last update Thu Feb 18 16:03:12 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	fill_my_field_bits(unsigned int bool, int i, u_charbin *charbin)
{
  if (i == 1)
    charbin->bin.one = bool;
  else if (i == 2)
    charbin->bin.two = bool;
  else if (i == 3)
    charbin->bin.three = bool;
  else if (i == 4)
    charbin->bin.four = bool;
  else if (i == 5)
    charbin->bin.five = bool;
  else if (i == 6)
    charbin->bin.six = bool;
  else if (i == 7)
    charbin->bin.seven = bool;
  else if (i == 8)
    {
      charbin->bin.eight = bool;
      my_putchar(charbin->c);
    }
  return (1);
}

int	lauch_server_secondconditon(u_charbin *charbin, int t)
{
  fill_my_field_bits(0, t, charbin);
  if (t == 8)
    {
      t = 0;
      if (charbin->c == '\0')
	g_stop = 0;
    }
  return (t);
}

int	lauch_server_conditon(u_charbin *charbin, int t)
{
  fill_my_field_bits(1, t, charbin);
  if (t == 8)
    {
      t = 0;
      if (charbin->c == '\0')
	g_stop = 0;
    }
  return (t);
}

void		lauch_server(u_decbin *decbin)
{
  static int	t = 0;
    u_charbin	charbin;
  int		sig;

  sig = -1;
  if  (g_stop == 0)
    recup_client_pid(decbin);
  else if ((sig = my_signal(0, 0)) == 1 || sig == 0)
    {
      if (sig == 1)
	{
	  t++;
	  t = lauch_server_conditon(&charbin, t);
	}
      else if (sig == 0)
	{
	  t++;
	  t = lauch_server_secondconditon(&charbin, t);
	}
      kill((pid_t)decbin->client_pid, SIGUSR1);
      my_signal(-1, 1);
    }
}

int	main()
{
  pid_t		pid_server;
  u_decbin	decbin;

  g_stop = 0;
  pid_server = getpid();
  my_putstr("On listen: ");
  my_putnbr(pid_server);
  my_putchar('\n');
  signal(SIGUSR1, handler);
  signal(SIGUSR2, handler);
  while (42)
    lauch_server(&decbin);
}
